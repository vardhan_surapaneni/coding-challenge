# UI Test

## Overview

Create a single page application with list of loans.

This test should ideally take 3-4 hours.

## Functional Requirements.

*   Refer to images for UX design.
*   Create a webapi to retrieve loans.
*   Display retrieved loans on the page as expandable cards as shown in the image.
*   Carryover amount need to be updated with selected account's carryover balance. 
*   `Apply for Increased Loan amounts` button should be enabled only when selected balance is more than `0`.
*   `Apply for new Loan` button need to be disabled when 3 or more accounts are available.


## Technical requirements.

*   Push your solution as a github repo.
*   Push your code as you go (don't push all changes to one commit.)

UI 

*   Use either `Angular`(`5+`), `ReactJs` or `VueJs`.
*   Should use Container Presentational Component Pattern.
*   Should use single store to maintain application state.
*   Components need to be updated when state changed.
*   Handle service errors and display on screen.
*   Create components wherever required.
*   Unit test all your components.
*   Responsive design.
*   Don't use any style libraries.

WebApi

* Use your own data store and don't hardcode loan accounts.
* Loan records can be manually entered into your data store.
* Api will be under heavy load in production hence the code need to be performant.
* It is expected to have error/exception handling mechanisms in place
* It would be great if you use design patterns in your application
* Test coverage is non-negotiable


